import { INCEMENT, DECEMENT } from '../actions/counterActions';

const initialCount = 0;

export const counterReducer = (state = initialCount, action) => {
    switch (action.type) {
        case INCEMENT:
            return state + action.payLoad
        case DECEMENT:
            return state - action.payLoad
        default:
            return state
    }
}