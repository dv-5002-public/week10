import { ADD_TODO, FETCH_TODO } from '../actions/todoAction';

const ADD_TODO = 'ADD_TODO'
const FETCH_TODO_BEGIN = 'FETCH_TODO_BEGIN'
const FETCH_TODO_SUCCESS = 'FETCH_TODO_SUCCESS'
const FETCH_TODO_ERROR = 'FETCH_TODO_ERROR'

const initialTodos = [];

// * version 1
// export const todoReducer = (state = initialTodos, action) => {
//     switch (action.type) {
//         case ADD_TODO:
//             const newTodo = {
//                 taskName: action.payLoad
//             }
//             return [newTodo, ...state]
//         default:
//             return state
//     }
// }

// * version 2
export const todoReducer = (state = initialTodos, action) => {
    switch (action.type) {
        case ADD_TODO:
            const newTodo = {
                taskName: action.payLoad,
                complete: false
            }
            return [newTodo, ...state]
        // return [action.payLoad, ...state];
        case FETCH_TODO_BEGIN:
            return {
                ...state,
                loading: true
            }
        case FETCH_TODO_SUCCESS:
            return {
                todos:action.payLoad,
                loading: false,
                
            }
        case FETCH_TODO_ERROR:
            return {
                ...state,
                loading: true
            }
        default:
            return state;
    }
}

export const fetchTodoSuccess = () => {

}