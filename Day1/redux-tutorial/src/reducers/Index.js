import { combineReducers } from "redux";
import { counterReducer } from '../reducers/counterReducer'
import { todoReducer } from "./TodoReducer";

// export const rootReducer = combineReducers({
//     counterReducer
// })

export const rootReducer = combineReducers({
    counter: counterReducer,
    todosCompState: todoReducer
})