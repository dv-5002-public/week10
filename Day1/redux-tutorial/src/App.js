import React from 'react';
import './App.css';
import Counter from './component/Counter';
import CounterRedux from './component/CounterRedux';
import TodoRedux from './component/TodoRedux';

function App() {
  return (
    <div className="App">
      <Counter />
      <CounterRedux />
      <TodoRedux />
    </div>
  );
}

export default App;
