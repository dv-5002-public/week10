import React, { useEffect } from 'react';
import TodoForm from './TodoForm';
import TodoList from './TodoList';
import { useDispatch, useSelector } from 'react-redux';
import { fetchTodos } from '../actions/todoAction';
import { fetchTodoSuccess } from '../reducers/TodoReducer';

const TodoRedux = () => {
    // const dispatch = useDispatch();
    const todos = useSelector()
    useEffect(() => {
        fetchTodos()
    }, []);

    const fetchTodos = () => {
        return dispatch => {
            dispatch(fetchTodoBegin())
            fetch('')
                .then(res => res.json())
                .then(data => {
                    dispatch(fetchTodoSuccess(data))
                })
                .catch(error => dispatch(fetchTodoError(error)))
        }
    }

    return (
        <div>
            <h1>Todo Redux</h1>
            <div>
                <TodoForm />
                <TodoList />
            </div>
        </div>
    );
}

export default TodoRedux;
