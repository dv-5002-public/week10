import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { decrement, increment } from '../actions/counterActions';

const CounterRedux = () => {
    const counter = useSelector(state => state.counter);

    const dispatch = useDispatch();

    return (
        <div>
            <h1>Counter Redux</h1>
            <div>Count = {counter}</div>
            <div>
                <button onClick={() => dispatch(decrement(5))}>-</button>
                <button onClick={() => dispatch(increment(5))}>+</button>
            </div>
        </div>
    );
}

export default CounterRedux;
