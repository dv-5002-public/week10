import React, { useState } from 'react';
import { addTodo } from '../actions/todoAction';
import { useDispatch } from 'react-redux';

const TodoForm = () => {
    const [newTodoName, setNewTodoName] = useState('');

    const dispatch = useDispatch();

    const onclickedAddHandle = () => {
        dispatch(addTodo(newTodoName));
        setNewTodoName("");
    };

    return (
        <div>
            <input type="text" value={newTodoName} onChange={(e) => setNewTodoName(e.target.value)} />
            <button onClick={onclickedAddHandle}>ADD</button>
        </div>
    );
}

export default TodoForm;