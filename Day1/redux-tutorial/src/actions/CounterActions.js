export const INCEMENT = 'INCEMENT'
export const DECEMENT = 'DECEMENT'

export const increment = (value) => {
    const action = {
        type: INCEMENT,
        payLoad: value
    }

    return action
}

export const decrement = (value) => {
    const action = {
        type: DECEMENT,
        payLoad: value
    }

    return action
}