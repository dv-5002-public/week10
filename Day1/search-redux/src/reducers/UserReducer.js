import { FETCH_USER } from '../actions/TaskActions'

const initialTodos = []

export const UserReducer = (state = initialTodos, action) => {
    switch (action.type) {
        case FETCH_USER:
            return action.payLoad
        default:
            return state
    }
}