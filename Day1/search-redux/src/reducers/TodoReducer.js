import { FETCH_TODO } from '../actions/TaskActions'

const initialTodos = []

export const TodoReducer = (state = initialTodos, action) => {
    switch (action.type) {
        case FETCH_TODO:
            return action.payLoad
        default:
            return state
    }
}