import { SELECT_TODO } from '../actions/TaskActions'

const initialTodos = -1

export const SelectReducer = (state = initialTodos, action) => {
    switch (action.type) {
        case SELECT_TODO:
            return action.payLoad
        default:
            return state
    }
}