import { SEARCH_TODO } from '../actions/TaskActions'

const initialTodos = ''

export const SearchReducer = (state = initialTodos, action) => {
    switch (action.type) {
        case SEARCH_TODO:
            return action.payLoad
        default:
            return state
    }
}