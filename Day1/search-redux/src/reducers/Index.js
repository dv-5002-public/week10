import { combineReducers } from 'redux'
import { UserReducer } from './UserReducer'
import { TodoReducer } from './TodoReducer'
import { SelectReducer } from './SelectReducer'
import { SearchReducer } from './SearchReducer'
import { DoneReducer } from './DoneReducer'

export const rootReducer = combineReducers({
    userInfo: UserReducer,
    todos: TodoReducer,
    select: SelectReducer,
    search: SearchReducer,
    done: DoneReducer
})