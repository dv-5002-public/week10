import { DONE_TODO } from '../actions/TaskActions'

const initialTodos = []

export const DoneReducer = (state = initialTodos, action) => {
    switch (action.type) {
        case DONE_TODO:
            return action.payLoad
        default:
            return state
    }
}