import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import * as serviceWorker from './serviceWorker';
import { BrowserRouter, Route, Redirect } from 'react-router-dom'
import 'antd/dist/antd.css'
import { createStore } from 'redux';
import { rootReducer } from './reducers/Index';
import { Provider } from 'react-redux'
import UserPage from './pages/UserPage';
import TodoPage from './pages/TodoPage';
import AlbumUserPage from './pages/AlbumUserPage';
import AlbumListPage from './pages/AlbumListPage';

const MainRouting =
    <BrowserRouter>
        <Route exact path="/" render={() => (
            <Redirect to="/users" />
        )} />

        <Route path="/users" component={UserPage} exact={true} />
        <Route path="/users/:user_id/todo" component={TodoPage} />
        <Route path="/users/:user_id/albums" component={AlbumUserPage} exact={true} />
        <Route path="/users/:user_id/albums/:id" component={AlbumListPage} />
    </BrowserRouter>

const appStore = createStore(
    rootReducer,
    window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__())

ReactDOM.render(
    <Provider store={appStore}>
        {MainRouting}
    </Provider>
    , document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
