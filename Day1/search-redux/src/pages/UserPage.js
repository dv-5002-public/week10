import React from 'react';
import { List, Avatar, Layout, Skeleton, Input } from 'antd';

import reqwest from 'reqwest';
const { Search } = Input;

const { Content } = Layout;

class UserPage extends React.Component {

    state = {
        data: [],
        list: [],
        users: [],
        searchText: ''
    };

    fetchUserData = () => {
        fetch('https://jsonplaceholder.typicode.com/users')
            .then(response => response.json())
            .then(data => {
                this.setState({ users: data })
            })
            .catch(error => console.log(error))
    }

    componentDidMount() {
        this.fetchUserData()
        this.getData(res => {
            this.setState({
                data: res.results,
                list: res.results,
            });
        });
    }

    getData = callback => {
        reqwest({
            type: 'json',
            method: 'get',
            contentType: 'application/json',
            success: res => {
                callback(res);
            },
        });
    };


    render() {
        const { initLoading } = this.state;
        return (
            <Content style={{ padding: '50px 50px' }}>
                <Search
                    placeholder="input search text"
                    onChange={event => { this.setState({ searchText: event.target.value }) }}
                    style={{ width: 200 }}
                    onKeyUp={this.filterName}
                />
                <List
                    className="demo-loadmore-list"
                    loading={initLoading}
                    itemLayout="horizontal"
                    dataSource={this.state.searchText !== '' ? this.state.users.filter(m => m.name.match(this.state.searchText
                    )) :
                        this.state.users}
                    renderItem={item => (
                        <List.Item
                            actions={[<a href={"/users/" + item.id + "/todo"}>Todo</a>, <a href={"/users/" + item.id + "/albums"}>Albums</a>]}
                        >
                            <Skeleton avatar title={false} loading={item.loading} active>
                                <List.Item.Meta
                                    avatar={
                                        <Avatar src="https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png" />
                                    }
                                    title={<a href="https://ant.design">{item.name}</a>}
                                    description={item.email}
                                />
                                <div>{item.phone} | {item.website}</div>
                            </Skeleton>
                        </List.Item>
                    )}
                />
            </Content>
        );
    }
}

export default UserPage