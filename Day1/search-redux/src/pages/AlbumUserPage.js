import React, { useState, useEffect } from 'react';
import { List, Layout } from 'antd';

const AlbumUserPage = (props) => {
    const [user, setUser] = useState({})
    const [albumUserList, setAlbumUserList] = useState([])
    const { Content } = Layout;

    const fetchUserData = () => {
        const userId = props.match.params.user_id
        fetch('https://jsonplaceholder.typicode.com/users/' + userId)
            .then(response => response.json())
            .then(data => {
                setUser(data)
            })
            .catch(error => console.log(error))
    }

    const fetchAlbumData = () => {
        const userId = props.match.params.user_id
        fetch('https://jsonplaceholder.typicode.com/albums?userId=' + userId)
            .then(response => response.json())
            .then(data => {
                setAlbumUserList(data)
            })
            .catch(error => console.log(error))
    }

    //componentDidMount
    useEffect(() => {
        fetchUserData()
        fetchAlbumData()
    }, [])


    return (
        <Content style={{ padding: '50px 50px' }}>
            <List
                header={<div>Albums</div>}
                footer={<div>Item: {albumUserList.length}</div>}
                bordered
                dataSource={albumUserList}
                renderItem={item => (
                    <List.Item actions={[<a href={"/users/" + user.id + "/albums/" + item.id}>{item.title}</a>]} />
                )}
            />
        </Content>
    )
}

export default AlbumUserPage