import React, { useState, useEffect } from 'react';
import { List, Typography, Button, Descriptions, Select, Layout, Input } from 'antd';
import { useSelector, useDispatch } from 'react-redux';
import { fetchUsers, fetchTodos, selectTodos, searchTodos, doneTodos } from '../actions/TaskActions';

const TodoPage = (props) => {
    const { Option } = Select;
    // const [user, setUser] = useState([]);
    const [todoList, setTodoList] = useState([]);
    // const [selected, setSelected] = useState(-1);
    const { Content } = Layout;
    const { Text } = Typography;
    const { Search } = Input;

    const dispatch = useDispatch()
    const userInfo = useSelector(state => state.userInfo)
    const todos = useSelector(state => state.todos)
    const select = useSelector(state => state.select)
    const search = useSelector(state => state.search)
    const done = useSelector(state => state.done)

    useEffect(() => {
        fetchUsersData();
        fetchTodoData();
    }, [])

    const fetchUsersData = () => {
        const userId = props.match.params.user_id;
        fetch('http://jsonplaceholder.typicode.com/users/' + userId)
            .then(response => response.json())
            .then(data => {
                dispatch(fetchUsers(data))
            })
            .catch(error => console.log(error));
    }

    const fetchTodoData = () => {
        const userId = props.match.params.user_id;
        fetch('http://jsonplaceholder.typicode.com/todos?userId=' + userId)
            .then(response => response.json())
            .then(data => {
                dispatch(fetchTodos(data))
            })
            .catch(error => console.log(error));
    }

    const doneStatus = (value) => {
        let newList = [...todoList];
        newList[value].completed = false;
        setTodoList(newList)
    }

    const onSelectChange = (value) => {
        // alert(value)
        dispatch(selectTodos(value))
        console.log(value)
    }

    return (
        <Content style={{ padding: '50px 50px' }}>
            <Descriptions title="User Info" >
                <Descriptions.Item label="Name">{userInfo.name}</Descriptions.Item>
                <Descriptions.Item label="User Name">{userInfo.username}</Descriptions.Item>
                <Descriptions.Item label="Email">{userInfo.email}</Descriptions.Item>
                <Descriptions.Item label="Telephone">{userInfo.phone}</Descriptions.Item>
            </Descriptions>
            <Select defaultValue="All" style={{ width: 120 }} onChange={onSelectChange}>
                <Option value={-1}>All</Option>
                <Option value={true}>Done</Option>
                <Option value={false}>Doing</Option>
            </Select>

            <Search
                placeholder="input search text"
                onChange={event => { this.setState({ searchText: event.target.value }) }}
                style={{ width: 200 }}
            />

            <List
                header={<div>Todo List</div>}
                footer={<div>Footer</div>}
                bordered
                dataSource={
                    select === -1 ?
                        todos
                        :
                        todos.filter(m => m.completed === select)
                }
                renderItem={(item, index) => (
                    <List.Item>
                        <Typography.Text mark={item.completed} delete={!item.completed}>
                            {item.completed ?
                                <Text>DOING</Text>
                                :
                                <Text>DONE</Text>}
                        </Typography.Text>

                        {" " + item.title}

                        {item.completed &&
                            <Button value={index} style={{ float: "right" }} onClick={() => doneStatus(item.id - 1)}>Done</Button>}}
                </List.Item>
                )}
            />
        </Content>
    )
}
export default TodoPage;