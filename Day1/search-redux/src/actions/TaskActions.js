export const FETCH_USER = 'FETCH_USER'
export const FETCH_TODO = 'FETCH_TODO'
export const SELECT_TODO = 'SELECT_TODO'
export const DONE_TODO = 'DONE_TODO'
export const SEARCH_TODO = 'SEARCH_TODO'

export const fetchUsers = (taskName) => {
    return {
        type: FETCH_USER,
        payLoad: taskName
    }
}

export const fetchTodos = (data) => {
    return {
        type: FETCH_TODO,
        payLoad: data 
    }
}

export const selectTodos = (data) => {
    return {
        type: SELECT_TODO,
        payLoad: data
    }
}

export const searchTodos = (data) => {
    return {
        type: SEARCH_TODO,
        payLoad: data
    }
}

export const doneTodos = (data) => {
    return {
        type: DONE_TODO,
        payLoad: data
    }
}