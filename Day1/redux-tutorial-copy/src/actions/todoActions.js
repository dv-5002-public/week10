export const ADD_TODO = 'ADD_TODO'
export const FETCH_TODOS = 'FETCH_TODOS'

export const addTodo = (taskName) => {
    return {
        type: ADD_TODO,
        payLoad:  taskName 
    }
}

export const fetchTodo = () => {
    return {
        type: FETCH_TODOS,
    }
}
