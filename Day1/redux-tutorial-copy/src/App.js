import React from 'react';
import './App.css';
import Counter from './components/Counter';
import CounterRedux from './components/CounterRedux';
import TodoRedux from './components/TodoRedux';

function App() {

  return (
    <div className="App">
      <Counter />
      <CounterRedux/>
      <TodoRedux/>
    </div>
  );
}

export default App;
