import { INCREMENT, DECREMENT } from "../actions/counterActions"

const initialCount = 0

export const counterReducer = (state = initialCount, action) => {
    switch (action.type) {
        case INCREMENT:
            return state + action.payLoad
        case DECREMENT:
            return state - action.payLoad
        default:
            return state
    }
}