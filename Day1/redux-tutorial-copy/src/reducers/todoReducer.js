const ADD_TODO = 'ADD_TODO'
const FETCH_TODOS_BEGIN = 'FETCH_TODOS_BEGIN'
const FETCH_TODOS_SUCCESS = 'FETCH_TODOS_SUCCESS'
const FETCH_TODOS_ERROR = 'FETCH_TODOS_ERROR'

export const addTodo = (taskName) => {
    return {
        type: ADD_TODO,
        payLoad: taskName
    }
}

export const fetchTodos = () => {
    return dispatch => {
        dispatch(fetchTodoBegin())
        return fetch('http://jsonplaceholder.typicode.com/todos')
            .then(res => res.json())
            .then(data => {
                dispatch(fetchTodoSuccess(data))
            })
            .catch(error => dispatch(fetchTodoError(error)))
    }
}

export const fetchTodoBegin = () => {
    return {
        type: FETCH_TODOS_BEGIN,
    }
}

export const fetchTodoSuccess = todos => {
    return {
        type: FETCH_TODOS_SUCCESS,
        payLoad: todos
    }
}
export const fetchTodoError = error => {
    return {
        type: FETCH_TODOS_ERROR,
        payLoad: error
    }
}

const initialState = {
    todos: [],
    loading: false,
    error: ''
}

export const todoReducer = (state = initialState, action) => {
    switch (action.type) {
        case ADD_TODO:
            const newTodo = {
                taskName: action.payLoad,
                completed: false
            }
            return [newTodo, ...state]
        case FETCH_TODOS_BEGIN:
            return {
                ...state,
                loading: true
            }
        case FETCH_TODOS_SUCCESS:
            return {
                todos: action.payLoad,
                loading: false,
                error: ''
            }
        case FETCH_TODOS_ERROR:
            return {
                ...state,
                loading: false,
                error: action.payLoad
            }
        default:
            return state
    }
}
