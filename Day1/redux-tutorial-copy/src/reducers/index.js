import { combineReducers } from "redux";
import { counterReducer } from './couterReducer'
import { todoReducer } from "./todoReducer";

// export const rootReducer = combineReducers({
//     counterReducer
// })

export const rootReducer = combineReducers({
    counter : counterReducer,
    todosCompState : todoReducer
})
