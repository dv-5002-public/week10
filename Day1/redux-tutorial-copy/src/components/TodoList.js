import React from 'react';
import { useSelector } from 'react-redux';

const TodoList = () => {

    const todos = useSelector(state => state.todosCompState.todos)

    return (
        <div>
            {
                todos.map((todo) => {
                    return <div>
                        <h4>{todo.userId}</h4>
                        <h4>{todo.title}</h4>
                        <h4>{todo.id}</h4>
                        <h4>{todo.completed}</h4>
                    </div>
                })
            }
        </div>
    )
}

export default TodoList;