import React from 'react';
import { useSelector, useDispatch, connect } from 'react-redux';
import { increment, decrement } from '../actions/counterActions';
import { bindActionCreators } from 'redux';

const CounterRedux = (props) => {

    // const counter = useSelector(state => state.counter)

    // const dispatch = useDispatch();

    const { counter, increment, decrement } = props

    return (
        <div>
            <h1>Counter Redux</h1>
            <div>Counts = {counter}</div>
            <div>
                <button onClick={() => decrement(5)}>-</button>
                <button onClick={() => increment(5)}>+</button>
            </div>
        </div>
    );
}

const mapStateToProps = state => {
    return {
        counter: state.counter
    }
}

const mapDispatchToProps = dispatch => {
    return bindActionCreators({ increment, decrement }, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(CounterRedux);
