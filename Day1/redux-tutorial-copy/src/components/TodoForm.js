import React, { useState } from 'react';
import { useDispatch } from 'react-redux';
import { addTodo } from '../actions/todoActions';

const TodoForm = () => {

    const [newTodoName, setNewTodoName] = useState('')

    const dispatch = useDispatch();

    const clickedAddHandler = () => {
        dispatch(addTodo(newTodoName))
        setNewTodoName('')
    }

    return (
        <div>
            <input placeholder='New Todo' value={newTodoName} onChange={(e) =>
                setNewTodoName(e.target.value)} />
            <button onClick={clickedAddHandler}>ADD</button>
        </div>
    )
}


export default TodoForm;
