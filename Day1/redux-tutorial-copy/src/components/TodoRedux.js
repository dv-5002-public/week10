import React, { useEffect } from 'react';
import TodoForm from './TodoForm';
import TodoList from './TodoList';
import { useDispatch } from 'react-redux';
import { fetchTodos } from '../reducers/todoReducer';
const TodoRedux = () => {

    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(fetchTodos())
    }, [])

    return (
        <div>
            <h1>Todo Redux</h1>
            <div>
                <TodoForm />
                <TodoList />
            </div>
        </div>
    );
}

export default TodoRedux;
