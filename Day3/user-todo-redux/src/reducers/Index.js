import { combineReducers } from "redux"
import { TodoReducer } from "./TodoReducer"
import { UsersReducer } from "./UsersReducer"

export const rootReducer = combineReducers({
    todoState: TodoReducer,
    userState: UsersReducer
})