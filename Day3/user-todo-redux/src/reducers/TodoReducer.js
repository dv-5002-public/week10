import { DONE_TODO, SELECT_TODO, SEARCH_TODO, FETCH_TODO_BEGIN, FETCH_TODO_SUCCESS, FETCH_TODO_ERROR, FETCH_USER_BEGIN, FETCH_USER_SUCCESS, FETCH_USER_ERROR } from "../actions/TodoAction"

const initialState = {
    user: {},
    todoList: [],
    selectedTodoFilter: -1,
    keyWord: '',
    loadingTodo: false,
    loadingUser: false,
    error: ''
}

export const TodoReducer = (state = initialState, action) => {
    switch (action.type) {
        case DONE_TODO:
            const temp = [...state.todoList]
            for (let i = 0; i < temp.length; i++) {
                if (temp[i].id === action.todoId) {
                    temp[i].completed = true
                }
            }
            return {
                ...state,
                todoList: temp
            }
        case SELECT_TODO:
            return {
                ...state,
                selectedTodoFilter: action.value
            }
        case SEARCH_TODO:
            return {
                ...state,
                keyWord: action.keyWord
            }
        case FETCH_TODO_BEGIN:
            return {
                ...state,
                loadingTodo: true
            }
        case FETCH_TODO_SUCCESS:
            return {
                ...state,
                todoList: action.payLoad,
                loadingTodo: false,
                error: ''
            }
        case FETCH_TODO_ERROR:
            return {
                ...state,
                loadingTodo: false,
                error: action.payLoad
            }
        case FETCH_USER_BEGIN:
            return {
                ...state,
                loadingUser: true
            }
        case FETCH_USER_SUCCESS:
            return {
                ...state,
                user: action.payLoad,
                loadingUser: false,
                error: ''
            }
        case FETCH_USER_ERROR:
            return {
                ...state,
                loadingUser: false,
                error: action.payLoad
            }
        default:
            return state
    }
}
