import { SEARCH_USER, FETCH_USERS_BEGIN, FETCH_USERS_SUCCESS, FETCH_USERS_ERROR } from "../actions/UsersAction"

const initialState = {
    keyWord: '',
    users: [],
    loading: false,
    error: ''
}

export const UsersReducer = (state = initialState, action) => {
    switch (action.type) {
        case SEARCH_USER:
            return {
                ...state,
                keyWord: action.keyWord
            }
        case FETCH_USERS_BEGIN:
            return {
                ...state,
                loading: true
            }
        case FETCH_USERS_SUCCESS:
            return {
                ...state,
                users: action.payLoad,
                loading: false
            }
        case FETCH_USERS_ERROR:
            return {
                ...state,
                loading: false,
                error: action.payLoad
            }
        default:
            return state
    }
}