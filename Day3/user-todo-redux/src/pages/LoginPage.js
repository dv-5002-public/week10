import React from 'react'
import { Icon, Input, Button, Form, Layout, Card, Col } from 'antd'

const LoginPage = (props) => {
    const { getFieldDecorator } = props.form
    const { Content } = Layout

    const handleSubmit = e => {
        e.preventDefault();
        props.form.validateFields((err, values) => {
            if (!err) {
                console.log('Received values of form: ', values)
                window.location = '/processLogin/' + values["username"] + values["password"]
            }
        });
    };

    return (
        <Content style={{ padding: '150px 50px', minHeight: "100vh", background: "#EEEEEE" }}>
            <Form onSubmit={handleSubmit} className="login-form">
                <Col offset={9}>
                    <Card
                        title="Login Form"
                        bordered={false}
                        hoverable
                        style={{ width: '40%' }}
                        actions={[
                            <Button type="primary" htmlType="submit" className="login-form-button">
                                <Icon type="login" />Log in
                            </Button>
                        ]}>
                        <Form.Item>
                            {getFieldDecorator('username', {
                                rules: [{ required: true, message: 'Please input your username!' }],
                            })(
                                <Input
                                    prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />}
                                    placeholder="Username"
                                />,
                            )}
                        </Form.Item>
                        <Form.Item>
                            {getFieldDecorator('password', {
                                rules: [{ required: true, message: 'Please input your Password!' }],
                            })(
                                <Input
                                    prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />}
                                    type="password"
                                    placeholder="Password"
                                />,
                            )}
                        </Form.Item>
                    </Card>
                </Col>
            </Form>
        </Content>
    )
}

const WrappedLoginForm = Form.create({ name: 'normal_login' })(LoginPage);

export default WrappedLoginForm