import React from 'react'
import { Input } from 'antd'
import { connect } from 'react-redux'
import { searchTodo } from '../../actions/TodoAction'
import { bindActionCreators } from 'redux'

const SearchTodo = (props) => {
    const { Search } = Input
    const { searchTodo } = props

    const handleChange = ({ target }) => {
        searchTodo(target.value)
    }

    return (
        <Search
            placeholder="Search Todo(s)"
            onChange={handleChange}
            style={{ width: '300px', marginRight: '5px' }}
        />
    )
}

const mapStateToProp = (state) => {
    return {}
}

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({ searchTodo }, dispatch)
}

export default connect(mapStateToProp, mapDispatchToProps)(SearchTodo)
