import React from 'react'
import { List, Avatar, Skeleton, Row } from 'antd'
import { connect } from 'react-redux'
import SearchUser from './SearchUser'

const UsersList = (props) => {
    const { handleKeyword, users, isLoading } = props

    return (
        <List
            style={{ backgroundColor: 'white', borderRadius: '10px', padding: '0px 30px' }}
            loading={isLoading}
            className="demo-loadmore-list"
            itemLayout="horizontal"
            header={
                <Row type="flex" justify="start">
                    <SearchUser />
                </Row>
            }
            dataSource={
                handleKeyword === "" ?
                    users
                    :
                    users.filter(user => user.name.toUpperCase().match(handleKeyword.toUpperCase()))
            }
            renderItem={item => (
                <List.Item
                    actions={
                        [
                            <a href={"/users/" + item.id + "/todo"}>Todo</a>
                        ]}>
                    <Skeleton avatar title={false} loading={item.loading} active>
                        <List.Item.Meta
                            avatar={
                                <Avatar src="https://image.flaticon.com/icons/svg/1177/1177568.svg" />
                            }
                            title={item.name}
                            description={item.email}
                        />
                        <div>{item.phone} | {item.website}</div>
                    </Skeleton>
                </List.Item>
            )}
        />
    )
}

const mapStateToProps = (state) => {
    return {
        handleKeyword: state.userState.keyWord,
        users: state.userState.users,
        isLoading: state.userState.loading
    }
}

export default connect(mapStateToProps)(UsersList)
