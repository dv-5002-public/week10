import React from 'react'
import { Input } from 'antd'
import { connect } from 'react-redux'
import { searchUser } from '../../actions/UsersAction'
import { bindActionCreators } from 'redux'

const SearchUser = (props) => {
    const { Search } = Input
    const { searchUser } = props

    const handleChange = ({ target }) => {
        searchUser(target.value)
    }

    return (
        <Search
            placeholder="Search User(s)"
            onChange={handleChange}
            style={{ width: '300px' }}
            size="large"
        />
    )
}

const mapStateToProps = (state) => {
    return
}

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({ searchUser }, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(SearchUser)
