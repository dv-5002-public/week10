import React from 'react';
import TodoToolbar from '../components/todo/TodoToolbar'
import TodoList from '../components/todo/TodoList'


const TodoPage = () => {
    return (
        <div>
            <TodoToolbar />
            <TodoList />
        </div>
    )

}

export default TodoPage