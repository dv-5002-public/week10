import React from 'react';
import logo from './logo.svg';
import './App.css';
import TodoPage from '../src/pages/TodoPage';

function App() {
  return (
    <div>
      <TodoPage />
    </div>
  );
}

export default App;
